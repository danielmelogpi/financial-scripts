Scripts para gestão financeira
=============
> Eu morro de preguiça de lançar isso na mão 

## Extrair fatura do cartão

Uso:

- Na pagina do Itau, use CTRL+A CTRL+C e cole todo o conteudo em `res/dump-fatura.txt`
- Executar:
```js
node ./extrair-fatura-cartao.js
```
- Uma planilha sera criada com os lançamentos em `result/planilha.xlsx`
- O formato da planilha segue a importação do Organizze
