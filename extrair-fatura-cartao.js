var XLSX = require('xlsx')

var lineReader = require("readline").createInterface({
  input: require("fs").createReadStream("res/dump-fatura.txt")
});


var lancamentosRaw = []
var startLancamentos = false
lineReader.on("line", function(line) {
    if (line.startsWith('Crédito do cartão final')){
        startLancamentos = false
    }
    if (startLancamentos) {
        lancamentosRaw.push(line)
    }
    if (line.startsWith('Lançamentos nacionais')) {
        startLancamentos = true
    }
}).on('close', () => {
    lancamentosRaw.shift()
    lancamentosRaw.shift()
    writeXLS(lancamentosRaw.map((line) => {
        let [date, desc, value] = line.split('\t')
        date = date + '/2018'
        value = Number(value.replace(',', '.')) * (-1)
        return [
            date,
            desc,
            getCategory(desc),
            value
        ]
    }))
});

function writeXLS(values) {
    var sheetName = "SheetJS";
    
    var titleLine = 
        ["Data","Descrição","Categoria","Valor","Situação"]

    var sheetData = [titleLine, ...values]

    var ws = XLSX.utils.aoa_to_sheet(sheetData)
    var wb = XLSX.utils.book_new()
    
    XLSX.utils.book_append_sheet(wb, ws, sheetName)
    XLSX.writeFile(wb, 'result/planilha.xls')
}


function getCategory(description) {
    if ((/Uber/g).test(description)) {
        return 'Uber / Outros'
    } else if ((/NETFLIX|SPOTIFY/g).test(description)) {
        return 'Entretenimento online'
    } else if ((/ALURA/g).test(description)) {
        return 'Educação'
    } else if ((/ORGANIZZE/g).test(description)) {
        return 'Despesas essenciais da casa'
    } else if ((/POST/g).test(description)) {
        return 'Combustível'
    }
    return ''
}